#include "CWordList.h"

CWordList :: CWordList(string i_strFileName)
{
    m_nLineNumber = 0;
    m_nMaxLineNumber = -1;

    ifstream file(i_strFileName.c_str());
    string strData = "";

    while ( file )
    {
        getline(file,strData,'\n');
        struct SData sData;
        sData.m_strWord = strData.substr(0,17);
        sData.m_strMeaning = strData.substr(17);
        m_nMaxLineNumber ++;
        m_vecWords.push_back(sData);
    }
}

string CWordList :: getWord(int i_nLineNumber)
{
    if ( i_nLineNumber <= m_nMaxLineNumber )
    {
        return m_vecWords[i_nLineNumber].m_strWord;
    }

    return "";
}

string CWordList :: getMeaning(int i_nLineNumber)
{
    if ( i_nLineNumber <= m_nMaxLineNumber )
    {
        return m_vecWords[i_nLineNumber].m_strMeaning;
    }

    return "";
}

string CWordList :: getMeaning(string i_strWord)
{
    for ( int nLoop = 0 ; nLoop < m_vecWords.size() ; nLoop ++ )
    {
        if ( m_vecWords[nLoop].m_strWord == i_strWord )
        {
            return m_vecWords[nLoop].m_strMeaning;
        }
    }

    return "";
}

string CWordList :: getCurrentWord()
{
    if ( m_nLineNumber <= m_nMaxLineNumber )
    {
        return m_vecWords[m_nLineNumber].m_strWord;
    }

    return "";
}

string CWordList :: getCurrentMeaning()
{
    if ( m_nLineNumber <= m_nMaxLineNumber )
    {
        return m_vecWords[m_nLineNumber].m_strMeaning;
    }

    return "";
}

// pre increment ++ operator
CWordList & CWordList :: operator ++()
{
    if ( m_nLineNumber < m_nMaxLineNumber )
        m_nLineNumber ++;
    return *this;
}

// post increment ++ operator
CWordList & CWordList :: operator ++(int )
{
    if ( m_nLineNumber < m_nMaxLineNumber )
        m_nLineNumber ++;
    return *this;
}

// pre increment -- operator
CWordList & CWordList :: operator --()
{
    if ( m_nLineNumber > 0 )
        m_nLineNumber --;
    return *this;
}

// post increment -- operator
CWordList & CWordList :: operator --(int )
{
    if ( m_nLineNumber > 0 )
        m_nLineNumber --;
    return *this;
}
