#ifndef CWORDLIST_H
#define CWORDLIST_H

#include <string>
#include <fstream>
#include <vector>

using namespace std;

struct SData
{
    string m_strWord;
    string m_strMeaning;
};

class CWordList
{
    public:
    CWordList(string i_strFileName);
    string getWord(int i_nLineNumber);
    string getMeaning(int i_nLineNumber);
    string getCurrentWord();
    string getCurrentMeaning();
    string getMeaning(string i_strWord);
    CWordList & operator ++();
    CWordList & operator --();
    CWordList & operator ++(int );
    CWordList & operator --(int );
    int getSize(){return m_nMaxLineNumber+1;}

    private:
    int m_nLineNumber;
    int m_nMaxLineNumber;
    vector<SData> m_vecWords;
};


#endif // CWORDLIST_H
