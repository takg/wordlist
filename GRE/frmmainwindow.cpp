#include "frmmainwindow.h"
#include "ui_frmmainwindow.h"
#include <qtimer.h>

using namespace std;

FrmMainWindow::FrmMainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::FrmMainWindowClass)
{
    ui->setupUi(this);

    m_ptheCWordList = 0;
    m_theFileDialog.setFileMode(QFileDialog::ExistingFile);

    QObject::connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(onOpen()));
    QObject::connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(update()));
    QObject::connect(ui->listWords, SIGNAL(itemSelectionChanged()), this, SLOT(onWordClicked()));
    QObject::connect(ui->txtWord, SIGNAL(textChanged(const QString &)), this, SLOT(onWordEntered()));
    QObject::connect(ui->txtStartWords, SIGNAL(textChanged(const QString &)), this, SLOT(populateFromFile()));

    populateFromFile();
}

FrmMainWindow::~FrmMainWindow()
{
    if ( m_ptheCWordList != 0 )
    {
        delete m_ptheCWordList;
        m_ptheCWordList = 0;
    }
    delete ui;
}

void FrmMainWindow :: update()
{
    ofstream file("D:\\Programming\\Qt\\GRE\\status.txt");
    file<<ui->listWords->currentRow();

    return;
}

void FrmMainWindow :: onOpen()
{
    m_theFileDialog.exec();
    QString strFileName;
    QStringList strFileList=m_theFileDialog.selectedFiles();
    if ( strFileList.count() > 0 )
    {
        strFileName = strFileList.at(0);
        populateFromFile(string(strFileName.toLatin1()));
    }
}

void FrmMainWindow :: onWordClicked()
{
    if ( ui->chkMeaning->isChecked() == false)
    {
        ui->txtMeaning->clear();
        return;
    }

    int nLine = ui->listWords->currentRow();

    string strMeaning = m_ptheCWordList->getMeaning((const char *)ui->listWords->currentItem()->text().toAscii());

    ui->txtMeaning->setText(
            QString(strMeaning.c_str())
            +"\nLine No :: "
            +QString::number(nLine+1));
    return;
}

void FrmMainWindow :: onWordEntered()
{
    QString strWord = ui->txtWord->text();
    string stringWord = string(strWord.toLatin1());
    int nLength = stringWord.length();
    int nLoop = 0;

    for ( nLoop = 0 ; nLoop < m_ptheCWordList->getSize() ; nLoop ++ )
    {
        if ( m_ptheCWordList->getWord(nLoop).substr(0,nLength).compare(stringWord) >= 0 )
        {
            break;
        }
    }

    ui->listWords->setCurrentRow(nLoop);

    return;
}

void FrmMainWindow :: populateFromFile()
{
    populateFromFile("D:\\Programming\\Qt\\GRE\\list.txt");
    return;
}

void FrmMainWindow :: populateFromFile(string i_strFileName)
{
    if ( m_ptheCWordList != 0 )
    {
        delete m_ptheCWordList;
        m_ptheCWordList = 0;
    }

    m_ptheCWordList = new CWordList(i_strFileName);
    ui->listWords->clear();

    for ( int nLoop = 0 ; nLoop < m_ptheCWordList->getSize() ; nLoop ++ )
    {
        if ( ui->txtStartWords->text() != "" )
        {
            QString strChild = m_ptheCWordList->getWord(nLoop).c_str();
            QString strParent = ui->txtStartWords->text();
            if ( strParent.contains(strChild.at(0)) == false )
            {
                continue;
            }
        }

        ui->listWords->addItem(QString(m_ptheCWordList->getWord(nLoop).c_str()));
    }

    int nLoc=-1;
    ifstream file("D:\\Programming\\Qt\\GRE\\status.txt");
    if ( file )
    {
        file>>nLoc;
        if ( nLoc > 0 && ui->listWords->count() > nLoc)
        {
            ui->listWords->setCurrentRow(nLoc);
        }
    }
}
