#ifndef FRMMAINWINDOW_H
#define FRMMAINWINDOW_H

#include <QtGui/QMainWindow>
#include "CWordList.h"
#include "qfiledialog.h"

namespace Ui
{
    class FrmMainWindowClass;
}

class FrmMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    FrmMainWindow(QWidget *parent = 0);
    ~FrmMainWindow();

public slots:
    void onOpen();
    void onWordClicked();
    void onWordEntered();
    void update();
    void populateFromFile();
    void populateFromFile(string i_strFile);

private:
    Ui::FrmMainWindowClass *ui;
    CWordList *m_ptheCWordList;
    QFileDialog m_theFileDialog;
};

#endif // FRMMAINWINDOW_H
