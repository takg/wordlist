/********************************************************************************
** Form generated from reading ui file 'frmmainwindow.ui'
**
** Created: Thu 17. Dec 22:11:13 2009
**      by: Qt User Interface Compiler version 4.4.3
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_FRMMAINWINDOW_H
#define UI_FRMMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGroupBox>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FrmMainWindowClass
{
public:
    QAction *actionExit;
    QAction *actionOpen;
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QGroupBox *grpWords;
    QLabel *label;
    QLineEdit *txtWord;
    QTextEdit *txtMeaning;
    QListWidget *listWords;
    QCheckBox *chkMeaning;
    QLineEdit *txtStartWords;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *FrmMainWindowClass)
    {
    if (FrmMainWindowClass->objectName().isEmpty())
        FrmMainWindowClass->setObjectName(QString::fromUtf8("FrmMainWindowClass"));
    FrmMainWindowClass->resize(471, 557);
    QIcon icon;
    icon.addPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/query.png")), QIcon::Normal, QIcon::Off);
    FrmMainWindowClass->setWindowIcon(icon);
    actionExit = new QAction(FrmMainWindowClass);
    actionExit->setObjectName(QString::fromUtf8("actionExit"));
    actionOpen = new QAction(FrmMainWindowClass);
    actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
    centralWidget = new QWidget(FrmMainWindowClass);
    centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
    groupBox = new QGroupBox(centralWidget);
    groupBox->setObjectName(QString::fromUtf8("groupBox"));
    groupBox->setGeometry(QRect(10, 0, 451, 501));
    grpWords = new QGroupBox(groupBox);
    grpWords->setObjectName(QString::fromUtf8("grpWords"));
    grpWords->setGeometry(QRect(10, 14, 431, 471));
    label = new QLabel(grpWords);
    label->setObjectName(QString::fromUtf8("label"));
    label->setGeometry(QRect(10, 20, 71, 16));
    txtWord = new QLineEdit(grpWords);
    txtWord->setObjectName(QString::fromUtf8("txtWord"));
    txtWord->setGeometry(QRect(10, 40, 201, 20));
    txtMeaning = new QTextEdit(grpWords);
    txtMeaning->setObjectName(QString::fromUtf8("txtMeaning"));
    txtMeaning->setGeometry(QRect(220, 70, 201, 391));
    txtMeaning->setReadOnly(true);
    listWords = new QListWidget(grpWords);
    listWords->setObjectName(QString::fromUtf8("listWords"));
    listWords->setEnabled(true);
    listWords->setGeometry(QRect(10, 70, 201, 391));
    chkMeaning = new QCheckBox(grpWords);
    chkMeaning->setObjectName(QString::fromUtf8("chkMeaning"));
    chkMeaning->setGeometry(QRect(220, 40, 131, 18));
    chkMeaning->setChecked(true);
    txtStartWords = new QLineEdit(grpWords);
    txtStartWords->setObjectName(QString::fromUtf8("txtStartWords"));
    txtStartWords->setGeometry(QRect(322, 40, 101, 20));
    FrmMainWindowClass->setCentralWidget(centralWidget);
    menuBar = new QMenuBar(FrmMainWindowClass);
    menuBar->setObjectName(QString::fromUtf8("menuBar"));
    menuBar->setGeometry(QRect(0, 0, 471, 21));
    menuFile = new QMenu(menuBar);
    menuFile->setObjectName(QString::fromUtf8("menuFile"));
    FrmMainWindowClass->setMenuBar(menuBar);
    mainToolBar = new QToolBar(FrmMainWindowClass);
    mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
    FrmMainWindowClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
    statusBar = new QStatusBar(FrmMainWindowClass);
    statusBar->setObjectName(QString::fromUtf8("statusBar"));
    FrmMainWindowClass->setStatusBar(statusBar);

    menuBar->addAction(menuFile->menuAction());
    menuFile->addAction(actionOpen);
    menuFile->addSeparator();
    menuFile->addAction(actionExit);

    retranslateUi(FrmMainWindowClass);
    QObject::connect(actionExit, SIGNAL(triggered()), FrmMainWindowClass, SLOT(close()));

    QMetaObject::connectSlotsByName(FrmMainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *FrmMainWindowClass)
    {
    FrmMainWindowClass->setWindowTitle(QApplication::translate("FrmMainWindowClass", "FrmMainWindow", 0, QApplication::UnicodeUTF8));
    actionExit->setText(QApplication::translate("FrmMainWindowClass", "Exit", 0, QApplication::UnicodeUTF8));
    actionOpen->setText(QApplication::translate("FrmMainWindowClass", "Open", 0, QApplication::UnicodeUTF8));
    groupBox->setTitle(QApplication::translate("FrmMainWindowClass", "GRE", 0, QApplication::UnicodeUTF8));
    grpWords->setTitle(QApplication::translate("FrmMainWindowClass", "Words", 0, QApplication::UnicodeUTF8));
    label->setText(QApplication::translate("FrmMainWindowClass", "Look for :", 0, QApplication::UnicodeUTF8));
    chkMeaning->setText(QApplication::translate("FrmMainWindowClass", "Show Meaning", 0, QApplication::UnicodeUTF8));
    menuFile->setTitle(QApplication::translate("FrmMainWindowClass", "File", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class FrmMainWindowClass: public Ui_FrmMainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FRMMAINWINDOW_H
